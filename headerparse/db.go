package headerparse

import (
	"database/sql"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

func CreateDb() {
	db, err := sql.Open("sqlite3", "./test.db")
	if err != nil {
		Log.Fatal(err)
	}
	_, err = db.Exec(
		`CREATE TABLE IF NOT EXISTS ErrorDefs (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            Identifier VARCHAR(32) NULL,
            Token VARCHAR(32) NULL,
            Comment VARCHAR(400) NULL,
            FoundIn VARCHAR(260) NULL,
            LineNumber INT
        );`,
	)
	if err != nil {
		Log.Fatal(err)
	}
	_, err = db.Exec(
		`CREATE TABLE IF NOT EXISTS Aliases (
            Token VARCHAR(32),
            Alias VARCHAR(32)
        );`,
	)
	if err != nil {
		Log.Fatal(err)
	}
	err = db.Close()
	if err != nil {
		Log.Fatal(err)
	}
}
