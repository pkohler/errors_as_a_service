package headerparse

import (
	"bufio"
	"errors"
	"fmt"
	"math/big"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var (
	reErrDef        = regexp.MustCompile(`(?-s)#define\s+(\S+)\s+(_HRESULT_|NDIS_ERROR)?.*?(TYPEDEF_)?.*?(0[xX][0-9a-fA-F]+|\d+)`)
	reErrIdentifier = regexp.MustCompile(`(^E_)|(_E_)|(ERROR)`)
	reCommentSingle = regexp.MustCompile(`//.*`)
	reCommentStart  = regexp.MustCompile(`/\*.*`)
	reCommentEnd    = regexp.MustCompile(`\*/`)
	reBlankLine     = regexp.MustCompile(`^\s*$`)
	errNotAComment  = errors.New("no comment found on line")
)

func buildComment(line string, comment string, multi bool) (string, bool) {
	/* checks to see if the line contains a comment, and returns:
	 * - the string containing the comment that was found
	 * - a boolean indicating whether the comment should continue
	 */
	if reBlankLine.MatchString(line) {
		// ignore blank lines
		return comment, false
	} else if reCommentSingle.MatchString(line) {
		c := reCommentSingle.FindString(line)
		if c == "// dderror" {
			// nullify useless comments MS has sprinkled around
			return "", false
		} else {
			// build onto the comment we had previously. This'll be a blank string if there was none
			// and a real string if a bunch of comments were chained together in the file (much like
			// this comment is)
			return comment + reCommentSingle.FindString(line) + "\n", false
		}
	} else if reCommentStart.MatchString(line) {
		// start a new multiline comment (and end it if /* and */ are on the same line)
		return reCommentStart.FindString(line) + "\n", !reCommentEnd.MatchString(line)
	} else if multi {
		// build onto the multiline
		if reCommentEnd.MatchString(line) {
			return comment + reCommentEnd.FindString(line) + "\n", false
		} else {
			return comment + line + "\n", true
		}
	} else {
		return "", false
	}
}

func hexToStrInt(hexstr string) string {
	bi := big.NewInt(0)
	bi.SetString(strings.Replace(hexstr, "0x", "", 1), 16)
	return bi.String()
}

func intToHex(num int) string {
	return "0x" + strconv.FormatInt(int64(num), 16)
}

func trimHex(hexstr string) string {
	return "0x" + strings.TrimLeft(strings.Replace(hexstr, "0x", "", 1), "0")
}

func padHex(hexstr string) string {
	tmpstr := strings.Replace(hexstr, "0x", "", 1)
	tmpstr = "0x" + strings.Repeat("0", 8-len(tmpstr)) + tmpstr
	return tmpstr
}

func fileExt(filename string) string {
	split := strings.Split(filename, ".")
	return split[len(split)-1]
}

func ParseFile(path string, info os.FileInfo, err error) error {
	if info.IsDir() || fileExt(info.Name()) != "h" {
		// skip anything that isn't a header file
		return err
	}
	f, err := os.Open(path)
	r := bufio.NewReader(f)
	var (
		readErr error
		comment string
		line    string
		linenum int
		multi   bool
		ed      *ErrDef
	)
	comment = ""

	for readErr == nil {
		linenum++
		line, readErr = r.ReadString('\n')
		if reErrDef.MatchString(line) && reErrIdentifier.MatchString(line) {
			h := reErrDef.FindStringSubmatch(line)
			aliases := [3]string{"", "", ""}
			if i, err := strconv.Atoi(h[4]); err == nil {
				// token was originally int
				aliases[0] = h[4]
				aliases[1] = intToHex(i)
				if len(aliases[1]) < 10 {
					aliases[2] = padHex(aliases[1])
				}
			} else {
				// token was originally hex
				aliases[0] = hexToStrInt(h[4])
				aliases[1] = h[4]
				if len(aliases[1]) < 10 {
					aliases[2] = padHex(aliases[1])
				} else if aliases[1] != trimHex(aliases[1]) {
					aliases[2] = trimHex(aliases[2])
				}
			}
			ed = &ErrDef{
				Identifier: h[1],
				Token:      h[4],
				Aliases:    aliases,
				Comment:    comment,
				FoundIn:    f.Name(), //strings.Replace(f.Name(), parseDir, "", 1),
				LineNumber: linenum,
			}
			ed.SaveToDb()
			fmt.Printf("\n%v", ed.Json())
		}
		comment, multi = buildComment(line, comment, multi)
	}
	return err
}
