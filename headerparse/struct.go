package headerparse

import (
	"database/sql"
	"encoding/json"
	"encoding/xml"
	"fmt"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"os"
)

var Log = log.New(os.Stdout, "", log.LstdFlags|log.Lshortfile)

type ErrDef struct {
	Identifier string
	Token      string
	Aliases    [3]string
	Comment    string
	FoundIn    string
	LineNumber int
}

func (e *ErrDef) String() string {
	return fmt.Sprintf(
		"Token: %s\nIdentifier: %s\nAliases: %v\nComment: %s\nFoundIn: %s\nLineNumber: %d\n",
		e.Token,
		e.Identifier,
		e.Aliases,
		e.Comment,
		e.FoundIn,
		e.LineNumber,
	)
}

func (e *ErrDef) Json() string {
	js, err := json.Marshal(e)
	if err != nil {
		Log.Fatal(err)
	}
	return string(js)
}

func (e *ErrDef) Xml() string {
	x, err := xml.Marshal(e)
	if err != nil {
		Log.Fatal(err)
	}
	return string(x)
}

func (e *ErrDef) SaveToDb() {
	// TODO: touch this up so it can work on either sqlite or postgres
	db, err := sql.Open("sqlite3", "./test.db")
	if err != nil {
		Log.Fatal(err)
	}
	stmt, err := db.Prepare(
		"INSERT INTO ErrorDefs(Identifier, Token, Comment, FoundIn, LineNumber) values(?,?,?,?,?);",
	)
	if err != nil {
		Log.Fatal(err)
	}
	_, err = stmt.Exec(e.Identifier, e.Token, e.Comment, e.FoundIn, e.LineNumber)
	if err != nil {
		Log.Fatal(err)
	}
	stmt, err = db.Prepare("INSERT INTO Aliases(Token, Alias) values(?,?);")
	if err != nil {
		Log.Fatal(err)
	}
	permutations := [6][2]string{
		// manually assigning is faster than running the perms programatically, and there's only 6
		{e.Aliases[0], e.Aliases[1]},
		{e.Aliases[0], e.Aliases[2]},
		{e.Aliases[1], e.Aliases[0]},
		{e.Aliases[1], e.Aliases[2]},
		{e.Aliases[2], e.Aliases[0]},
		{e.Aliases[2], e.Aliases[1]},
	}
	for _, a := range permutations {
		if a[0] != "" && a[1] != "" {
			_, err = stmt.Exec(a[0], a[1])
			if err != nil {
				Log.Fatal(err)
			}
		}
	}
	err = db.Close()
	if err != nil {
		Log.Fatal(err)
	}
}

func checkErr(err error) {
	// TODO: should handle errors better than just killing everything on any err
	if err != nil {
		Log.Fatal(err)
	}
}
