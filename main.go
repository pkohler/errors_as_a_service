package main

import (
	"bitbucket.org/pkohler/erraas/headerparse"
	"path/filepath"
)

func main() {
	headerparse.CreateDb()
	var parseDir = "/data/repos/Windows Kits/"
	filepath.Walk(parseDir, headerparse.ParseFile)
}
